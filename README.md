# RUDI

Buildroot OS for Jetson Nano

## Requirements
### Fedora/Redhat
```
$ sudo dnf install perl-ExtUtils-MakeMaker perl-Thread-Queue
```

## Build Instructions
```
$ git submodule update --init --recursive
$ make BR2_EXTERNAL=$PWD/br-external -C buildroot jetson-nano_defconfig
$ make -C buildroot
```
